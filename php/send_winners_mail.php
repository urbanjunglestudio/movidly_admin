<?php

echo "<pre>".PHP_EOL;

if (!isset($_POST['email'])) {
	echo "Error: no mail";
	exit;
}

require_once 'mandrill-api-php/src/Mandrill.php';
$mandrill = new Mandrill('4r9gM2uJxPtYygEbgw8AaQ');

$template_name = 'MISSIONWINNER_FI';


$email = $_POST['email'];
$username = $_POST['username'];
$message = $_POST['message'];

$messageBR = str_replace("\n","<br />",$message);

$from = 'reward@whambush.com';
$subject = 'Onnea Whambush voitosta!';
$subjectMessage = 'Onnea '.$username."!";

$fromString = "Whambush-tiimi";

try {
    $message = array(
        'subject' => $subject,
        'from_email' => $from,
        'from_name' => 'Whambush',
        'to' => array(
            array(
                'email' => $email,
                'type' => 'to'
            )
        ),
        'important' => false,
        'track_opens' => true,
        'track_clicks' => true,
        'auto_text' => true,
        'auto_html' => false,
        'inline_css' => false,
        'url_strip_qs' => false,
        'preserve_recipients' => true,
        'view_content_link' => true,
        'bcc_address' => 'jari@whambush.com',
        'merge' => true,
        'merge_language' => 'mailchimp',
        'global_merge_vars' => array(
            array(
                'name' => 'SUBJECT',
                'content' => $subject
            ),
            array(
                'name' => 'WINNER_TITLE',
                'content' => $subjectMessage
            ),
            array(
                'name' => 'WINNER_CONTENT',
                'content' => $messageBR
            ),
            array(
                'name' => 'WINNER_FROM',
                'content' => $fromString
            )
        ),
        'tags' => array('mission_winner'),
        'google_analytics_domains' => array('whambush.com'),
        'recipient_metadata' => array(
            array(
                'rcpt' => $email,
                'values' => array('username' => $username)
            )
        ),
    );
    $async = false;
    $ip_pool = 'Main Pool';
    $send_at = '';

    $result = $mandrill->messages->sendTemplate($template_name, $template_content, $message, $async, $ip_pool, $send_at);

    if (isset($result['reject_reason'])) {
      echo "Error".PHP_EOL;
      print_r($result);
    } else {
      echo "Done".PHP_EOL;
      print_r($result);
    }

} catch(Mandrill_Error $e) {
    // Mandrill errors are thrown as exceptions
    echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
    // A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
    throw $e;
}

echo PHP_EOL."</pre>".PHP_EOL;


?>