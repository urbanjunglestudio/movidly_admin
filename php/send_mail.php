<?php

echo "<pre>".PHP_EOL;


$type = $_POST['type'];
$from = $_POST['from'];
$from_name = $_POST['from_name'];
$subject = $_POST['subject'];
$email = $_POST['email'];
$username = $_POST['username'];
$message = $_POST['message'];
if ($_POST['hide'] == "true"){
	$preserve = false;
} else {
	$preserve = true;
}

if (count($email) < 1 ) {
	echo "Error: no mail";
	exit;
}

require_once 'mandrill-api-php/src/Mandrill.php';
$mandrill = new Mandrill('4r9gM2uJxPtYygEbgw8AaQ');

$allemails = explode(",",$email);

if ( $type == "winner"){

	$template_name = 'MISSIONWINNER_FI';
	if	(count($allemails) > 1) {
		echo "Error: Only one address allowed in winner mail".PHP_EOL;
		exit; 
	} 
	$to = array(array('email' => $allemails[0],'type' => 'to'));
	$subject = 'Onnea Whambush voitosta!';
	$subjectMessage = 'Onnea '.$username."!";
	$from = 'reward@whambush.com';
	$from_name = "Whambush";
	$fromString = "Whambush-tiimi";
	$bcc_address = "jari@whambush.com";
	$tag = "mission_winner";	

} else {
	
	$template_name = 'MAIL_MESSAGE';
	for($i = 0; $i < count($allemails); $i++){
		$to[$i] = array('email' => $allemails[$i],'type' => 'to');
	} 
	$bcc_address = "";
	$tag = "send_mail";

}

$messageBR = str_replace("\n","<br />",$message);

try {
    $message = array(
        'subject' => $subject,
        'from_email' => $from,
        'from_name' => $from_name,
        'to' => $to,
        'important' => false,
        'track_opens' => true,
        'track_clicks' => true,
        'auto_text' => true,
        'auto_html' => false,
        'inline_css' => false,
        'url_strip_qs' => false,
        'preserve_recipients' => $preserve,
        'view_content_link' => true,
        'bcc_address' => $bcc_address,
        'merge' => true,
        'merge_language' => 'mailchimp',
        'global_merge_vars' => array(
            array(
                'name' => 'SUBJECT',
                'content' => $subject
            ),
            array(
                'name' => 'WINNER_TITLE',
                'content' => $subjectMessage
            ),
            array(
                'name' => 'WINNER_CONTENT',
                'content' => $messageBR
            ),
            array(
                'name' => 'MESSAGE_CONTENT',
                'content' => $messageBR
            ),
            array(
                'name' => 'WINNER_FROM',
                'content' => $fromString
            )
        ),
        'tags' => array($tag),
        'google_analytics_domains' => array('whambush.com'),
//        'recipient_metadata' => array(
//            array(
//                'rcpt' => $email,
//                'values' => array('username' => $username)
//            )
//        ),
    );
    $async = false;
    $ip_pool = 'Main Pool';
    $send_at = '';

    $result = $mandrill->messages->sendTemplate($template_name, $template_content, $message, $async, $ip_pool, $send_at);

    if (isset($result['reject_reason'])) {
      echo "Error".PHP_EOL;
      print_r($result);
    } else {
      echo "Done".PHP_EOL;
      print_r($result);
    }

} catch(Mandrill_Error $e) {
    // Mandrill errors are thrown as exceptions
    echo 'A mandrill error occurred: ' .PHP_EOL. get_class($e) .PHP_EOL. ' - ' .PHP_EOL. $e->getMessage();
    // A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
    throw $e;
}

echo PHP_EOL."</pre>".PHP_EOL;


?>